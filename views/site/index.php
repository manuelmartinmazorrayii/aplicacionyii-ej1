<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas con DAO y ORM</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <!--
            -->
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 1</h3>
                    <P>Listar las edades de los ciclistas (sin repetidos)</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta1orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta1dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 2</h3>
                    <P>Listar las edades de los ciclistas (sin repetidos) que pertenecen al equipo Artiach</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta2orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta2dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 3</h3>
                    <P>listar las edades de los ciclistas de Artiach o de Amore Vita</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta3orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta3dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 4</h3>
                    <P>listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta4orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta4dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 5</h3>
                    <P>listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta5orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta5dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 6</h3>
                    <P>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta6orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta6dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 7</h3>
                    <P>lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta7orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta7dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 8</h3>
                    <P>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta8orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta8dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 9</h3>
                    <P>Listar el nombre de los puertos cuya altura sea mayor de 1500 </P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta9orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta9dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 10</h3>
                    <P>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta10orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta10dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <h3>consulta 11</h3>
                    <P>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</P>
                    <p>
                        <?= Html::a('Active Record', ['site/consulta11orm'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta11dao'], ['class' => 'btn btn-default'])?>   
                    </p>
                </div>
            </div>
            
        </div>
    </div>
</div>
