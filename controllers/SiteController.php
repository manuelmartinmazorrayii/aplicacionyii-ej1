<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    /** CONSULTAS 1**/
    public function actionConsulta1dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(edad) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetido)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta1orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => Ciclista::find()->select("edad")->distinct(),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con active record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    /** CONSULTAS 2**/
        public function actionConsulta2dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(edad) from ciclista where nomequipo = "Artiach"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo = "Artiach"',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetido) que pertenecen al equipo artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Atriach'",
        ]);
    }
    
        public function actionConsulta2orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'"),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con active record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetido) que pertenecen al equipo artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    /** CONSULTAS 3**/
        public function actionConsulta3dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(edad) from ciclista where nomequipo = "Artiach" or nomequipo = "Amore Vita"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct edad from ciclista where nomequipo = "Artiach" or nomequipo = "Amore Vita"',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    
        public function actionConsulta3orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'")->orWhere("nomequipo = 'Amore Vita'"),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con active record",
            "enunciado"=>"listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    
    /** CONSULTAS 4**/
            public function actionConsulta4dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista where edad < 25 or edad > 30')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad < 25 or edad > 30',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
        ]);
    }
    
        public function actionConsulta4orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => Ciclista::find()->select("dorsal")->where("edad < 25")->orWhere("edad > 30"),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con active record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
        ]);
    }
    
    /** CONSULTAS 5**/
            public function actionConsulta5dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from ciclista where edad >= 28 and edad <= 32 and nomequipo = "Banesto"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from ciclista where edad >= 28 and edad <= 32 and nomequipo = "Banesto"',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad >= 28 AND edad <= 32 AND nomequipo = 'Banesto'",
        ]);
    }
    
        public function actionConsulta5orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => Ciclista::find()->select("dorsal")->where("edad < 32")->andWhere("edad > 28")->andWhere("nomequipo = 'Banesto'"),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con active record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad >= 28 AND edad <= 32 AND nomequipo = 'Banesto'",
        ]);
    } 
    
    /** CONSULTAS 6**/
    public function actionConsulta6dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(nombre) from ciclista where char_length(nombre) > 8')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct nombre from ciclista where char_length(nombre) > 8',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8",
        ]);
    }
    
        public function actionConsulta6orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => Ciclista::find()->select("nombre")->where("char_length(nombre) > 8"),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con active record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8",
        ]);
    }
    
    /** CONSULTAS 7**/
        public function actionConsulta7dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(nombre), dorsal, upper(nombre) as nommayus from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nombre, dorsal, upper(nombre) as nommayus from ciclista',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['nombre', 'dorsal', 'nommayus'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT nombre, dorsal, UPPER(nombre) AS nommayus FROM ciclista",
        ]);
    }
    
        public function actionConsulta7orm()
    {
        $dataProvider = new ActiveDataProvider([
    'query' => Ciclista::find()->select(['nombre','dorsal','UPPER(nombre) AS nommayus']),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['nombre', 'dorsal', 'nommayus'],
            "titulo"=>"Consulta 7 con active record",
            "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT nombre, dorsal, UPPER(nombre) AS nomMayus FROM ciclista",
        ]);
    }
    
    /** CONSULTAS 8**/
        public function actionConsulta8dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from lleva where código = "MGE"')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select distinct dorsal from lleva where código = "MGE"',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);
    }
    
        public function actionConsulta8orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => \app\models\Lleva::find()->select('dorsal')->distinct()->where('código = "MGE"'),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con active record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);
    }
    
    /** CONSULTAS 9**/
        public function actionConsulta9dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(nompuerto) from puerto where altura > 1500')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select nompuerto from puerto where altura > 1500',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
        ]);
    }
    
        public function actionConsulta9orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => \app\models\Puerto::find()->select('nompuerto')->where('altura > 1500'),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con active record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500",
        ]);
    }
    
    /** CONSULTAS 10**/
        public function actionConsulta10dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from puerto where pendiente > 8 or altura > 1800 and altura < 3000')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from puerto where pendiente > 8 or altura > 1800 and altura < 3000',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente > 8 OR altura > 1800 AND altura < 3000",
        ]);
    }
    
        public function actionConsulta10orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => \app\models\Puerto::find()->select('dorsal')->where('altura > 1800')->andWhere('altura < 3000')->orWhere('pendiente > 8'),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con active record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente > 8 OR altura > 1800 AND altura < 3000",
        ]);
    }
    /** CONSULTAS 11**/
            public function actionConsulta11dao()
    {
        $numero = Yii::$app->db
                ->createCommand('select count(dorsal) from puerto where pendiente > 8 and altura > 1800 and altura < 3000')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'select dorsal from puerto where pendiente > 8 and altura > 1800 and altura < 3000',
            'totalCount'=>$numero,
            'pagination'=>['pageSize' => 5,]
        ]);
        
        return $this->render("resultado", [
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente > 8 AND altura > 1800 AND altura < 3000",
        ]);
    }
    
        public function actionConsulta11orm()
    {
        $dataProvider = new ActiveDataProvider([
           'query' => \app\models\Puerto::find()->select('dorsal')->where('altura > 1800')->andWhere('altura < 3000')->andWhere('pendiente > 8'),
           'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con active record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente > 8 AND altura > 1800 AND altura < 3000",
        ]);
    }
}
